# Linear Actuator

ESP32 code to operate a linear actuator with a L298N h-bridge

PIN connections

L298N -- ESP32

ENA   -> 12

IN1   -> 14

IN2   -> 27

Additionally, ESP32 pins 34, 35 are connected to two push buttons, used for demo
purposes to extend and retract the actuator.