#include <Arduino.h>

#include <Wire.h>
#include "SSD1306Wire.h"

#include "AWSParameters.h"
#include "IOTGateway.h"
#include "StandardConfig.h"
#include <ArduinoJson.h>

const int ENABLE_MOTOR = 12;
const int IN1 = 14;
const int IN2 = 27;

const int BUTTON_EXTEND = 35;
const int BUTTON_RETRACT = 34;

SSD1306Wire display(0x3c, 5, 4);
char tempBuffer[100];

//full extension time with no load from fully retracted: 18.5 seconds
//full retraction time with no load from fully extended: 18 seconds

IOTGateway iotGateway(&capgeminiHackathon);

int currentValue = 0;

const int MAX_OPEN_TIME = 18000;

int msgReceived = 0;
String payload;
String rcvdPayload;
int latest;

bool change = true;
bool lightsOn = false;

void callBackHandler (char *topicName, int payloadLen, char *payLoad) {
  char rcvdPayloadChar[512];
  Serial.println("Callback invoked");
  strncpy(rcvdPayloadChar, payLoad, payloadLen);
  rcvdPayloadChar[payloadLen] = 0;
  rcvdPayload = rcvdPayloadChar;

  StaticJsonDocument<512> message;
  DeserializationError err = deserializeJson(message, rcvdPayload);

  if (err) {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(err.c_str());
  } else {
    latest = message["value"];
    msgReceived = 1;
  }
}

void retract()
{
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(ENABLE_MOTOR, HIGH);
}

void extend()
{
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(ENABLE_MOTOR, HIGH);
}

void stopActuator()
{
  digitalWrite(ENABLE_MOTOR, LOW);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, HIGH);
}

void displayValue(int value)
{
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 0, "Window");
  display.setFont(ArialMT_Plain_16);

  sprintf(tempBuffer, "%i %%", value);
  display.drawString(65, 0, tempBuffer);
  display.display();
}

void displayMessage(const char message[])
{
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 0, message);
  display.setFont(ArialMT_Plain_16);
  display.display();
}

void receiveMessage() {
  if(msgReceived == 1) {
    msgReceived = 0;
    change = true;
    Serial.print("Received: ");
    Serial.println(latest);
  }
}

void setup()
{
  Serial.begin(115200);
  pinMode(ENABLE_MOTOR, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(ENABLE_MOTOR, OUTPUT);
  pinMode(BUTTON_EXTEND, INPUT);
  pinMode(BUTTON_RETRACT, INPUT);

  display.init();
  display.flipScreenVertically();
  display.clear();

  displayMessage("IOT Init");
  iotGateway.initialise();
  delay(1000);
  displayMessage("Subscribe");
  iotGateway.subscribe("command/window", callBackHandler);
  displayMessage("Subscribed");
  delay(1000);

  displayMessage("Zeroing");
  retract();
  for (int i = 10; i >= 0; i--)
  {
    delay(1000);
    sprintf(tempBuffer, "Zeroing %is", i);
    displayMessage(tempBuffer);
  }
  stopActuator();
  displayMessage("Zeroed");
  currentValue = 0;
  Serial.println("Setup complete");
}

void actuate(int value){
  Serial.print("actuating ");
  Serial.println(value);
  if (currentValue!=value)
  {
    int difference = value - currentValue;
    int duration  = map(abs(difference), 0, 100, 0, MAX_OPEN_TIME);
    if (difference>=0)
    {
      Serial.print("Extending ");
      Serial.print(abs(difference));
      Serial.print(" by ");
      Serial.println(duration);
      extend();
    }
    else
    {
       Serial.print("Retracting ");
      Serial.print(abs(difference));
      Serial.print(" by ");
      Serial.println(duration);
      retract();
    }
    delay(duration);
    stopActuator();
    currentValue = value;
  }
  else
  {
    Serial.println("Nothing done");
  }
  
}

// the loop function runs over and over again forever
void loop()
{

  //read buttons
  int extendButton = digitalRead(BUTTON_EXTEND);
  int retractButton = digitalRead(BUTTON_RETRACT);

  if (extendButton == HIGH && retractButton == HIGH)
  {
    //stop the motor
    stopActuator();
  }

  if (extendButton == LOW)
  {
    Serial.println("Extend!");
    extend();
    delay(1000);
  }

  if (retractButton == LOW)
  {
    Serial.println("Retract!");
    retract();
    delay(1000);
  }

  receiveMessage();

  if (change)
  {
    sprintf(tempBuffer, "Going for %is", latest);
    displayMessage(tempBuffer);
    actuate(latest);
    displayValue(latest);
    change = false;
  }
  delay(250);
}
